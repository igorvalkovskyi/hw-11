//Немає сенсу їх використовувати так як для input є власні івенти(навіть якщо вони будуть працювати,їх використання некоректне)
let btns = document.querySelectorAll(".btn");
document.addEventListener("keydown", (event) => {
  for (let btn of btns) {
    btn.classList.remove("active");
    if (btn.dataset.key === event.key) btn.classList.add("active");
  }
});
